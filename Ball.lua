local Ball = class("Ball")

function Ball:initialize()
    self.pos = CENTER
    local initialSpeed = 300 -- px/s
    local randomDirection = math.random()
    self.movement = vector(initialSpeed, 0):rotated(randomDirection*TAU)
    self.radius = 20
    self.name = "Ball"
    self.color = {1.0, 1.0, 1.0}
    self.extraGlow = 0

    self.bounding_circle = {
        pos = self.pos,
        radius = self.radius
    }
end

function Ball:setColor(color)
    if self.color ~= self.extraGlow then
        self.color = color
        self.extraGlow = 2
    end
end

function Ball:draw()
    love.graphics.setColor(self.color)
    love.graphics.circle("fill", self.pos.x, self.pos.y, self.radius)
end

function Ball:drawGlow()
    love.graphics.setColor(self.color)
    love.graphics.draw(images.glow5, self.pos.x, self.pos.y, 0,0.5*(self.radius/20),0.5*(self.radius/20),images.glow5:getWidth()/2,images.glow5:getHeight()/2)
    love.graphics.setColor(0.7,0.7,0.7,1)
    love.graphics.draw(images.glow5, self.pos.x, self.pos.y, 0,0.3 + self.extraGlow,0.3 + self.extraGlow,images.glow5:getWidth()/2,images.glow5:getHeight()/2)
end

function Ball:update(dt, scene)
    -- shrinking for big balls:
    if self.radius > 20 then
        self.radius = self.radius - dt/3
    end
    -- movement and collisions
    local currentMovement = dt * self.movement

    local alreadyCollided = {}

    repeat
        -- compute bounding circle around pos and newPos
        local circle_pos = self.pos + currentMovement / 2
        local boundingCircle = {
            pos = circle_pos,
            radius = vector(circle_pos - self.pos):len() + self.radius
        }
        
        -- find nearest collision
        local minCollision = {
            hit = false,
            distanceToHit = currentMovement:len()
        }
        for _, thing in ipairs(scene) do
            if self ~= thing and thing.name ~= "Ball" and thing:isAlive() and alreadyCollided[thing] == nil and doCirclesIntersect(boundingCircle, thing.bounding_circle) then
                -- print ('Ball possibly touches ' .. thing.name)
                -- now compute how far the ball can go before it actually touches the thing
                local collision = thing:getCollisionWithBall(self, currentMovement)
                if collision.hit and collision.distanceToHit < minCollision.distanceToHit then
                    minCollision = collision
                end
            end
        end
        if minCollision.hit and minCollision.distanceToHit <= currentMovement:len() then
            minCollision.object:hit(self, minCollision)
            alreadyCollided[minCollision.object] = true
            -- reflect ball
            self.pos = minCollision.collisionPoint
            self.movement = (currentMovement - 2 * (currentMovement * minCollision.normal) * minCollision.normal):normalized() * self.movement:len()
            currentMovement = self.movement:normalized() * (currentMovement:len() - minCollision.distanceToHit)
        end
    until not minCollision.hit
    
    self.pos = self.pos + currentMovement
    self.bounding_circle.pos = self.pos

    if  self.extraGlow > 0 then
        self.extraGlow = math.max(0, self.extraGlow - dt * 10)
    end
end

function doCirclesIntersect(c1, c2)
   local centerDistance = (c1.pos - c2.pos):len()
   return centerDistance < c1.radius + c2.radius + 50
end

function Ball:isAlive()
    return true
end

return Ball
