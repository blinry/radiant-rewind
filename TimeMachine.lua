local TimeMachine = class("TimeMachine")

function TimeMachine:initialize(initialState)
    self.states = {deepcopy(initialState)}
    self.maxBufferSize = 60*60 --rewind max seconds (at 60fps)
end

function TimeMachine:push(scene)
    if #self.states >= self.maxBufferSize then
        table.remove(self.states, 1)
    end
    table.insert(self.states, deepcopy(scene))
end

function TimeMachine:pop()
    if self:hasPast() then
        return table.remove(self.states)
    else
        return deepcopy(self.states[1])
    end
end

function TimeMachine:hasPast()
    return #self.states > 1
end

-- Remaining rewind in percent.
function TimeMachine:remainingRewind()
    return #self.states/self.maxBufferSize
end

function TimeMachine:draw()
    local timeBarWidth = CANVAS_WIDTH/4
    love.graphics.setColor(0.5, 0.5, 0.5)
    love.graphics.rectangle("fill", 10, CANVAS_HEIGHT-40, timeBarWidth*timemachine:remainingRewind(), 30)
    love.graphics.setLineWidth(2)
    love.graphics.rectangle("line", 10, CANVAS_HEIGHT-40, timeBarWidth, 30, 5)

end

return TimeMachine
